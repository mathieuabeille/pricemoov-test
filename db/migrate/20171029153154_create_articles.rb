class CreateArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :articles do |t|
      t.references :user_id
      t.text :article
      t.timestamps
    end
  end
end
