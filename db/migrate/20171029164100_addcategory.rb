class Addcategory < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :category, :string
    add_column :articles, :headline, :string
    add_column :articles, :subhead, :string
  end
end
